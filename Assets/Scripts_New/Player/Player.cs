﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Character;

namespace Players
{
    [RequireComponent(typeof(Character_Movement))]
    [RequireComponent(typeof(Interactor))]
    [RequireComponent(typeof(Character_Inventory))]
    [RequireComponent(typeof(PlayerInputHandler))]

    public class Player : MonoBehaviour
    {
        [SerializeField] private string playerName = "Nameless";
        public string PlayerName { get => playerName; }
        public enum PlayerNumberEnum { Player_1, Player_2 }
        public PlayerNumberEnum PlayerNumber;

        public Character_Movement CharMovement { get; private set; }
        public Interactor CharInteractions { get; private set; }
        public Character_Inventory CharInventory { get; private set; }
        public PlayerInputHandler InputHandler { get; private set; }

        private bool componentsAreReady = false;

        public static Action<Player> PlayerAwakened;
        private bool _awakened = false;
        public static Action<Player> PlayerEnabled;
        public static Action<Player> PlayerDisabled;

        private void Awake()
        {
            RefreshComponents();
        }

        private void Start()
        {
            PlayerAwakened?.Invoke(this);
            _awakened = true;
        }

        private void OnEnable()
        {
            if (_awakened) PlayerEnabled?.Invoke(this);
        }

        private void OnDisable()
        {
            PlayerDisabled?.Invoke(this);
        }

        void RefreshComponents()
        {
            if (componentsAreReady) return;

            CharMovement = GetComponent<Character_Movement>();
            CharInteractions = GetComponent<Interactor>();
            CharInventory = GetComponent<Character_Inventory>();
            InputHandler = GetComponent<PlayerInputHandler>();
            componentsAreReady = true;
        }

        public void StopPayer()
        {
            CharMovement.StopMovementation(out bool successful);
            CharInteractions.InteractionsEnabled = false;
        }

        public void ResumePayer()
        {
            CharMovement.DefaultMovementation(out bool successful);
            CharInteractions.InteractionsEnabled = true;
        }
    }
}