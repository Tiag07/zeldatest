﻿using UnityEngine;
using System;
using Character;

namespace Players
{
    public class PlayerInputHandler : MonoBehaviour
    {
        [SerializeField] private Keymap keymap;
        [SerializeField] private Character_Movement charMovement;
        [SerializeField] private Character_Interactor charInteractor;

        public static bool ControlEnabled = true;

        public static Action StartPressed;

        public Keymap PlayerKeymap { get => keymap; set => keymap = value; }

        private void Start()
        {
            VerifyComponents();
        }

        void VerifyComponents()
        {
#if UNITY_EDITOR
            if (charMovement == null)
                Debug.LogWarning("InputHandler does not have CharacterMovement.", gameObject);

            if (charInteractor == null)
                Debug.LogWarning("InputHandler does not have CharacterInteractor.", gameObject);
#endif
        }

        private void Update()
        {
            if (ControlEnabled == false || charMovement.IsMovingAutomatically) return;

            if (charMovement)
            {
                MovementInputs();
                JumpInput();
            }

            if (charInteractor)
            {
                InteractInput();
            }

            PauseInput();
        }

        void MovementInputs()
        {
            float xAxis = 0;
            float yAxis = 0;

            if (Input.GetKey(PlayerKeymap.KeyUp))
            {
                yAxis = 1;
            }
            if (Input.GetKey(PlayerKeymap.KeyDown))
            {
                yAxis = -1;
            }
            if (Input.GetKey(PlayerKeymap.KeyLeft))
            {
                xAxis = -1;
            }
            if (Input.GetKey(PlayerKeymap.KeyRight))
            {
                xAxis = 1;
            }

            charMovement.MoveByInputs(xAxis, yAxis);
        }

        void JumpInput()
        {
            if (Input.GetKeyDown(PlayerKeymap.KeyJump))
            {
                charMovement.Jump();
            }
        }

        void InteractInput()
        {
            if (Input.GetKeyDown(PlayerKeymap.KeyInteract))
            {
                charInteractor.InteractWithObject();
            }
        }

        void PauseInput()
        {
            if (Input.GetKeyDown(PlayerKeymap.KeyStart))
            {
                StartPressed?.Invoke();
            }
        }
    }
}