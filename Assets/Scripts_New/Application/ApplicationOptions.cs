﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationOptions : MonoBehaviour
{
    public void QuitGame()
    {
        Application.Quit();
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    public void SetFrameRate()
    {
        Application.targetFrameRate += 10;

        if (Application.targetFrameRate >= 60)
        {
            Application.targetFrameRate = 20;
        }
    }
}
