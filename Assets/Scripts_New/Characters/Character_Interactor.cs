﻿using DetectionTriggers;
using System.Collections;
using UnityEngine;

namespace Character
{
    public class Character_Interactor : Interactor
    {
        [SerializeField] private OverlapTrigger interactorTrigger;

        private void OnEnable()
        {
            StartCoroutine(UpdateAsync());
        }

        IEnumerator UpdateAsync()
        {
            while (enabled)
            {
                yield return new WaitForSeconds(0.1f);
                if (InteractionsEnabled)
                {
                    FindInteractableObjects();
                }
            }
        }

        void FindInteractableObjects()
        {
            if (CanFindNewInteractables == false) return;

            var objDetected = interactorTrigger.GetFirstObjectHitted();
            if (objDetected == null)
            {
                NoInteractables();
                return;
            }
            else
            {
                var interactable = objDetected.GetComponent<IInteractable>();
                if (interactable != null)
                {
                    InteractableDetected?.Invoke(interactable.ActionName, objDetected);
                    CurrentInteractable = interactable;
                    return;
                }
            }

            void NoInteractables()
            {
                CurrentInteractable = null;
                InteractableDetected?.Invoke("", null);
            }
        }

        public void InteractWithObject()
        {
            if (InteractionsEnabled && CurrentInteractable != null)
            {
                CurrentInteractable.Interact(this);
                Interacted?.Invoke();
            }
        }

    }
}