﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CharacterMovementTypes;

namespace Character
{
    [RequireComponent(typeof(CharacterController))]
    public class Character_Movement : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private CharacterStats charStats;
        [SerializeField] private SharedLayerMask walkableLayers;
        public CharacterController CharController { get; private set; }
        public Vector3 CharControllerOriginalCenter { get; private set; }
        public float CharControllerOriginalHeight { get; private set; }
        private BoxCollider _headCollider;

        private Camera playerCamera;
        public Camera PlayerCamera { get => playerCamera; set { playerCamera = value; } }
        public bool ComponentsAreReady { get; private set; } = false;

        //Movement
        public bool IsMovingAutomatically { get; private set; } = false;
        public MovementType CurrentMoveType { get; private set; }
        public StandingMovement StandingMovement { get; private set; }
        public StaticMovement StaticMovement { get; private set; }

        //Gravity
        private bool _gravityEnabled = true;
        private float _gravityAcceleration;
        public bool IsGrounded { get; private set; } = false;

        [Header("Body Settings")]
        [SerializeField] private float stepCheckDistance = 0.4f; /** It prevents player changing to fall animation when it is just walking on step/slop */
        [SerializeField] private Vector3 groundCheckSphereOffset;

        public delegate bool AutoMoveStopCondition();
        private AutoMoveStopCondition _autoMoveStopCondition = null;
        private Vector3 _autoMoveLastPosition;
        private Vector3 _autoMoveDestination;
        private bool _autoMoveIgnoreAltitude = true;
        private float _autoMoveDistanceForStopping = 0.2f;

        public event Action<Vector3> Moving;
        public event Action<float, float> MoveInputReceived;
        public event Action<bool> Grounded;

        public event Action<Character_Movement> AutoMove_OnDestination;
        public event Action<Character_Movement> AutoMove_Interrupted;

        public event Action<MovementType> MovementStateStarted;
        public event Action<MovementType> MovementStateFinished;


        private void Awake()
        {
            RefreshComponents();
        }

        public void RefreshComponents()
        {
            if (ComponentsAreReady) return;

            CharController = GetComponent<CharacterController>();
            InitializeBodyAndMovementSettings();
            playerCamera = Camera.main;
            ComponentsAreReady = true;
        }

        void InitializeBodyAndMovementSettings()
        {
            CharControllerOriginalCenter = CharController.center;
            CharControllerOriginalHeight = CharController.height;

            StaticMovement = new StaticMovement();
            StandingMovement = new StandingMovement(this, CharControllerOriginalCenter, CharControllerOriginalHeight);
            CurrentMoveType = StandingMovement;
            CurrentMoveType.OnStateStarted();
            MovementStateStarted?.Invoke(CurrentMoveType);

            RefreshHeadColliderPosition();
        }

        public void RefreshHeadColliderPosition()
        {
            if (_headCollider == null) CreateHeadCollider();

            float headColliderY = CharController.bounds.max.y - _headCollider.size.y / 2;
            var boxPosition = new Vector3(CharController.bounds.center.x, headColliderY, CharController.bounds.center.z);
            _headCollider.transform.position = boxPosition;

            void CreateHeadCollider()
            {
                GameObject headObj = new GameObject("HeadCollider");
                headObj.layer = TagsAndLayers.Layer_IgnoreRaycast;
                _headCollider = headObj.AddComponent<BoxCollider>();

                _headCollider.transform.SetParent(transform);
                _headCollider.size = Vector3.one * CharController.radius * 2;
                RefreshHeadColliderPosition();
            }
        }

        public void StopMovementation(out bool successful)
        {
            SwitchMovementState(StaticMovement, out successful);
        }

        public void DefaultMovementation(out bool successful)
        {
            SwitchMovementState(StandingMovement, out successful);
        }

        public void InvokeMovementStateFinished() => MovementStateFinished?.Invoke(CurrentMoveType);
        public void InvokeMovementStateStarted() => MovementStateStarted?.Invoke(CurrentMoveType);
        public void SwitchMovementState(MovementType newMoveState, out bool successful, bool invokeOldStateFinishedAction = true, bool invokeNewStateStartedAction = true)
        {
            if (CurrentMoveType.CanExitFromThisState() && newMoveState.CanEnterInThisState())
            {
                CurrentMoveType.OnStateFinished();
                if (invokeOldStateFinishedAction) InvokeMovementStateFinished();

                CurrentMoveType = newMoveState;

                CurrentMoveType.OnStateStarted();
                if (invokeNewStateStartedAction) InvokeMovementStateStarted();

                successful = true;
            }
            else successful = false;
        }

        public void MoveByInputs(float xAxis, float yAxis)
        {
            MoveInputReceived?.Invoke(xAxis, yAxis);

            if (CurrentMoveType.AlternativeMovementation() == true) return;

            if (xAxis == 0 && yAxis == 0)
            {
                CharController.Move(Vector3.zero);
                Moving?.Invoke(Vector3.zero);
                return;
            }

            RotateCharacterViaInputs(xAxis, yAxis);
            MoveForward();
        }

        void RotateCharacterViaInputs(float xAxis, float yAxis)
        {
            if (CurrentMoveType.SpeedMultiplier() == 0) return;

            Vector2 moveInputs = new Vector2(xAxis, yAxis);

            float lookAngle = Mathf.Atan2(moveInputs.x, moveInputs.y) * Mathf.Rad2Deg + playerCamera.transform.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0, lookAngle, 0);
        }

        void Update()
        {
            CurrentMoveType.OnStateUpdated();
            GroundCheck();
            Gravity();

            if (IsMovingAutomatically)
            {
                AutoMove();
            }
        }

        void AutoMove()
        {
            bool onDestination = Vector3.Distance(transform.position, _autoMoveDestination) < _autoMoveDistanceForStopping;
            if (onDestination)
            {
                Teleport(_autoMoveDestination);
                Moving?.Invoke(Vector3.zero);
                AutoMove_OnDestination?.Invoke(this);
                IsMovingAutomatically = false;
                return;
            }

            if (_autoMoveIgnoreAltitude)
                _autoMoveDestination.y = transform.position.y;

            if (_autoMoveStopCondition.Invoke() == true)
            {
                InterruptProcess();
                return;
            }

            LookAtDestination(_autoMoveDestination);
            MoveForward();

            if (Time.timeScale > 0)
            {
                if (CharacterIsNotMoving())
                    InterruptProcess();

                _autoMoveLastPosition = transform.position;
            }

            bool CharacterIsNotMoving()
            {
                return _autoMoveLastPosition == transform.position;
            }

            void InterruptProcess()
            {
                AutoMove_Interrupted?.Invoke(this);
                Moving?.Invoke(Vector3.zero);
                IsMovingAutomatically = false;
            }
        }

        public void MoveAutomatically(Vector3 destination, AutoMoveStopCondition stopCondition = null, bool ignoreAltitude = true, float distanceForStopping = 0.2f)
        {
            _autoMoveDestination = destination;
            _autoMoveStopCondition = stopCondition;
            _autoMoveIgnoreAltitude = ignoreAltitude;
            _autoMoveDistanceForStopping = distanceForStopping;
            _autoMoveLastPosition = transform.position;
            IsMovingAutomatically = true;
        }

        void LookAtDestination(Vector3 destination)
        {
            float distance = Vector3.Distance(transform.position, destination);
            Quaternion lookRotation = Quaternion.LookRotation(destination - transform.position);

            if (distance > 2 && transform.rotation != lookRotation)
            {
                const float minLookRotationSpeed = 4f;
                const float maxLookRotationSpeed = 20f;
                float lookRotationSpeed = maxLookRotationSpeed * charStats.MoveSpeed / distance;
                lookRotationSpeed = Mathf.Clamp(lookRotationSpeed, minLookRotationSpeed, maxLookRotationSpeed);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, lookRotationSpeed * Time.deltaTime);
            }
            else
            {
                transform.rotation = lookRotation;
            }
        }

        public void MoveForward(bool negativeMovement = false)
        {
            float direction = negativeMovement ? -1 : 1;
            float moveTypeSpeed = charStats.MoveSpeed * CurrentMoveType.SpeedMultiplier() * direction;

            CharController.Move(transform.forward * moveTypeSpeed * Time.deltaTime);
            Moving?.Invoke(CharController.velocity);
        }

        public void Teleport(Vector3 destination)
        {
            CharController.enabled = false;
            transform.position = destination;
            CharController.enabled = true;
        }

        void Gravity()
        {
            if (!_gravityEnabled) return;

            if (IsGrounded && _gravityAcceleration < 0)
            {
                _gravityAcceleration = charStats.GravityForceWhenPlayerIsOnGround;
            }
            _gravityAcceleration += charStats.Gravity * Time.deltaTime;
            CharController.Move(new Vector3(0, _gravityAcceleration * Time.deltaTime, 0));
        }

        void GroundCheck()
        {
            IsGrounded = false;
            Vector3 checkerStartPosition = transform.position + transform.forward * groundCheckSphereOffset.z + transform.up * groundCheckSphereOffset.y;

            if (WalkableObjectInOverlapSphereDetected() || WalkingOnSteps())
            {
                IsGrounded = true;
            }

            Grounded?.Invoke(IsGrounded);

            bool WalkableObjectInOverlapSphereDetected()
            {
                Collider[] detectedObjects = Physics.OverlapSphere(checkerStartPosition, CharController.radius, walkableLayers.Layers, QueryTriggerInteraction.Ignore);

                foreach (var detectedObj in detectedObjects)
                {
                    if (detectedObj.gameObject == gameObject)
                        continue;
                    else
                        return true;
                }
                return false;
            }

            bool WalkingOnSteps()
            {
                RaycastHit[] stepCheckHits = Physics.RaycastAll(checkerStartPosition, Vector3.down, stepCheckDistance, walkableLayers.Layers, QueryTriggerInteraction.Ignore);

                foreach (var objectBellow in stepCheckHits)
                {
                    if (objectBellow.transform.gameObject == gameObject)
                        continue;
                    else
                        return true;
                }
                return false;
            }
        }

        public Collider[] GetObjectsInFrontOfFeets()
        {
            var charControllerBounds = CharController.bounds;
            var charControllerRadius = CharController.radius;

            var rayStartPoint = new Vector3(charControllerBounds.center.x, charControllerBounds.min.y + charControllerRadius, charControllerBounds.center.z);
            float rayDistance = charControllerRadius + 0.1f;

            var detectedObjects = Physics.RaycastAll(rayStartPoint, transform.forward, rayDistance, -1, QueryTriggerInteraction.Ignore);
            Debug.DrawRay(rayStartPoint, transform.forward * rayDistance, Color.red, Time.fixedDeltaTime);

            List<Collider> objectsInFrontOfFeets = new List<Collider>();

            foreach (var obj in detectedObjects)
            {
                if (obj.transform.gameObject == gameObject) continue;

                objectsInFrontOfFeets.Add(obj.collider);
            }

            return objectsInFrontOfFeets.ToArray();
        }

        public void Jump()
        {
            if (IsGrounded == false) return;
            if (CurrentMoveType.JumpEnabled() == false) return;

            _gravityAcceleration = Mathf.Sqrt(charStats.JumpForce * charStats.GravityForceWhenPlayerIsOnGround * charStats.Gravity);
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (CharController == null) CharController = GetComponent<CharacterController>();
#endif
            //Ground Sphere Check
            Vector3 spherePosition = transform.position + transform.forward * groundCheckSphereOffset.z + transform.up * groundCheckSphereOffset.y;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(spherePosition, CharController.radius);
            //Step Check Raycast
            Gizmos.DrawLine(spherePosition, spherePosition + Vector3.down * stepCheckDistance);
        }
    }
}