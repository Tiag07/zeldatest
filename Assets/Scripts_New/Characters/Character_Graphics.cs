﻿using UnityEngine;
using System;
using Character;
using UnityEditor;

public class Character_Graphics : MonoBehaviour
{
    [Header("Models are in Assets/Prefabs/Character Models.", order = 0)]
    [Header("Note: Set up the animator settings in model,", order = 1)]
    [Space(-10, order = 2)]
    [Header("only controller you will set here.", order = 3)]
    [Space(10, order = 4)]

    [SerializeField] private GameObject modelPrefab;
    private GameObject _currentModel;

    [SerializeField] private RuntimeAnimatorController animController;
    private Animator charAnimator;
    public Action<Animator> CharacterAnimatorIntantiated;

    private void Start()
    {
        RefreshModelAndAnimator();
    }

    public void RefreshModelAndAnimator()
    {
        RefreshCharacterModel();
        RefreshCharacterAnimator();
    }

    void RefreshCharacterModel()
    {

        if (modelPrefab == null) return;

        if (ModelExists() == false)
        {
            //No Model
            RefreshModel();
        }
#if UNITY_EDITOR
        else if (PrefabUtility.GetCorrespondingObjectFromSource(_currentModel) != modelPrefab)
        {
            //Different Model
            if (!Application.isPlaying)
                RefreshModel();
        }
        else
        {
            //Same Model
        }
#endif

    }

    bool ModelExists()
    {
        if (transform.childCount > 0)
        {
            _currentModel = ModelInChildreen();
        }

        if (_currentModel != null)
            return true;
        else
            return false;
    }

    public GameObject ModelInChildreen()
    {
        foreach (Transform child in transform)
        {
            var modelInstance = child.GetComponent<Character_ModelInstance>();
            if (modelInstance != null)
            {
                return modelInstance.gameObject;
            }
        }
        return null;
    }

    void RefreshModel()
    {
#if UNITY_EDITOR
        if (_currentModel != null)
        {
            DestroyImmediate(_currentModel);
        }

        var newObj = PrefabUtility.InstantiatePrefab(modelPrefab, transform) as GameObject;
        newObj.transform.position = transform.position;
        newObj.transform.rotation = transform.rotation;
        newObj.AddComponent<Character_ModelInstance>();
        _currentModel = newObj;
#endif
    }

    void RefreshCharacterAnimator()
    {
        charAnimator = _currentModel.GetComponent<Animator>();

        if (charAnimator != null)
        {
            if (animController != null)
            {
                charAnimator.runtimeAnimatorController = animController;
            }
            CharacterAnimatorIntantiated?.Invoke(charAnimator);
        }
    }


}
