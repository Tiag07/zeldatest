﻿using UnityEngine;

namespace Character
{
    [RequireComponent(typeof(Character_Inventory))]
    public class Character_Collector : MonoBehaviour, ICollector
    {
        private Character_Inventory _charInventory;
        public IInventory Inventory()
        {
            if (_charInventory == null)
            {
                _charInventory = GetComponent<Character_Inventory>();
            }
            return _charInventory;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == TagsAndLayers.Layer_Collectable)
            {
                var collectable = other.GetComponent<ICollectible>();
                if (collectable != null)
                {
                    collectable.Collected(this);
                }
            }
        }
    }
}