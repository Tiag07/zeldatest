﻿using System;
using UnityEngine;

namespace Character
{
    public class Character_Inventory : MonoBehaviour, IInventory
    {
        [SerializeField] private int crystals = 0;
        public event Action<int> CrystalValueChanged;

        public void AddCrystals(int value)
        {
            crystals += value;
            CrystalValueChanged?.Invoke(crystals);
        }

    }
}