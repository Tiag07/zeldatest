﻿using UnityEngine;

namespace DetectionTriggers
{
    public class OverlapSphereTrigger : OverlapTrigger
    {
        [SerializeField] private float sphereRadius = 0.3f;

        protected override void FindObjects()
        {
            DetectedObjects = Physics.OverlapSphere(GetTriggerPosition(), sphereRadius, DetectableLayers.Layers);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = GizmosColor;
            Gizmos.DrawWireSphere(GetTriggerPosition(), sphereRadius);
        }
    }
}