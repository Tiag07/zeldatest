﻿using UnityEngine;

namespace DetectionTriggers
{
    public class OverlapBoxTrigger : OverlapTrigger
    {
        [SerializeField] private Vector3 boxExtents = Vector3.one;
        protected override void FindObjects()
        {
            DetectedObjects = Physics.OverlapBox(GetTriggerPosition(), boxExtents / 2, transform.rotation, DetectableLayers.Layers);
        }

        protected override void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();

            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = GizmosColor;
            Gizmos.DrawWireCube(TriggerPositionOffset, boxExtents);
        }
    }
}