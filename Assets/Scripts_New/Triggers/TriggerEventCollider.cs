﻿using Managers;
using Players;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DetectionTriggers
{
    public class TriggerEventCollider : MonoBehaviour
    {
        [SerializeField] bool onlyForPlayers = true;
        [SerializeField] bool needAllPlayers = false;
        [SerializeField] SharedLayerMask layersThatCanActivate;
        [SerializeField] UnityEvent invokeOnTriggerEnter;
        [SerializeField] UnityEvent invokeOnTriggerExit;
        [SerializeField] List<Player> playersInside = new List<Player>();

        protected enum TriggerPhase { Enter, Exit }
        protected TriggerPhase CurrentTriggerPhase;


        protected virtual void OnTriggerEnter(Collider other)
        {
            Player player = null;
            if (onlyForPlayers && ColliderIsAPlayer(other, out player))
            {
                if (playersInside.Contains(player) == false)
                {
                    playersInside.Add(player);
                }
            }

            if (!TriggerIsAllowed(other, TriggerPhase.Enter, player)) return;
            invokeOnTriggerEnter?.Invoke();
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            Player player = null;
            if (onlyForPlayers && ColliderIsAPlayer(other, out player))
            {
                if (playersInside.Contains(player))
                {
                    playersInside.Remove(player);
                }
            }

            if (!TriggerIsAllowed(other, TriggerPhase.Exit, player)) return;
            invokeOnTriggerExit?.Invoke();
        }


        protected bool TriggerIsAllowed(Collider other, TriggerPhase phase, Player playerColliding = null)
        {
            if (ColliderIsInAllowedLayerMask(other) == false) return false;

            if (onlyForPlayers)
            {
                if (playerColliding != null)
                {
                    return CanActivate();
                }
                return false;
            }

            bool CanActivate()
            {
                bool interactionEnabled = playerColliding.CharInteractions.InteractionsEnabled;
                if (needAllPlayers)
                {
                    if (phase == TriggerPhase.Enter && AllPlayersAreInside())
                    {
                        return interactionEnabled;
                    }
                    else if (phase == TriggerPhase.Exit && playersInside.Count == 0)
                    {
                        return interactionEnabled;
                    }
                }
                else return interactionEnabled;

                return false;
            }

            return true;
        }

        bool ColliderIsInAllowedLayerMask(Collider other)
        {
            bool colliderIsInAllowedLayerMask = LayerMaskUtility.Exists(other.gameObject.layer, layersThatCanActivate.Layers);
            return colliderIsInAllowedLayerMask;
        }

        bool AllPlayersAreInside()
        {
            foreach (var playerInGame in GameManager.Instance.PlayersInGame)
            {
                if (playersInside.Contains(playerInGame) == false) return false;
            }
            return true;
        }

        bool ColliderIsAPlayer(Collider other, out Player playerColliding)
        {
            var playerComponent = other.GetComponent<Player>();
            playerColliding = playerComponent;
            return playerComponent != null;
        }
    }
}