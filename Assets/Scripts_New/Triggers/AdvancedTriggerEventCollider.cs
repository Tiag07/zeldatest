﻿using UnityEngine;
using UnityEngine.Events;

namespace DetectionTriggers
{
    public class AdvancedTriggerEventCollider : TriggerEventCollider
    {
        [SerializeField] UnityEvent invokeOnTriggerStay;

        protected virtual void OnTriggerStay(Collider other)
        {
            if (!TriggerIsAllowed(other, TriggerPhase.Enter)) return;
            invokeOnTriggerStay?.Invoke();
        }
    }
}