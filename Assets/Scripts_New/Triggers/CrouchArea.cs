﻿using Character;
using CharacterMovementTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DetectionTriggers
{
    public class CrouchArea : AdvancedTriggerEventCollider
    {
        public List<Character_Movement> charactersNotCrouched = new List<Character_Movement>();

        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);

            var charMovement = other.GetComponent<Character_Movement>();

            if (charMovement != null)
            {
                bool isCrouched = charMovement.CurrentMoveType is CrouchedMovement;

                if (!isCrouched && !charactersNotCrouched.Contains(charMovement))
                    charactersNotCrouched.Add(charMovement);
            }
        }

        protected override void OnTriggerStay(Collider other)
        {
            base.OnTriggerStay(other);

            if (charactersNotCrouched.Count > 0)
            {
                foreach (var character in charactersNotCrouched)
                {
                    if (character.IsGrounded)
                        StartCrouch(character);
                }
                charactersNotCrouched.RemoveAll(character => character.IsGrounded);
            }
        }

        protected override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);
            EndCrouch(other);
        }

        public void StartCrouch(Character_Movement charMovement)
        {
            charMovement.SwitchMovementState(new CrouchedMovement(charMovement), out bool successful);
        }

        public void EndCrouch(Collider other)
        {
            if (other.TryGetComponent(out Character_Movement charMovement))
            {
                charMovement.DefaultMovementation(out bool successful);
            }
        }
    }
}