﻿using UnityEngine;

namespace DetectionTriggers
{
    public abstract class OverlapTrigger : MonoBehaviour
    {
        [SerializeField] protected SharedLayerMask DetectableLayers;
        [SerializeField] protected Vector3 TriggerPositionOffset = new Vector3(0, 0.7f, 0.7f);
        [SerializeField] protected float RayStartPointAltitude = 1f;
        [SerializeField] protected Color GizmosColor = Color.red;
        [SerializeField] protected Collider[] DetectedObjects = new Collider[0];

        protected abstract void FindObjects();

        public Collider[] GetDetectedObjects()
        {
            FindObjects();
            return DetectedObjects;
        }

        public Collider GetFirstObjectHitted()
        {
            FindObjects();
            foreach (var obj in DetectedObjects)
            {
                if (DetectedObjects.Length == 0) return null;

                if (obj.gameObject == gameObject) continue;

                Vector3 startPosition = transform.position + Vector3.up * RayStartPointAltitude;
                Vector3 targetPosition = obj.bounds.center - startPosition;
                RaycastHit objectHitted;
                float rayDistance = 20f;
                int allLayers = -1;

                Physics.Raycast(startPosition, targetPosition, out objectHitted, rayDistance, allLayers, QueryTriggerInteraction.Ignore);
                Debug.DrawLine(startPosition, objectHitted.point, GizmosColor, 0.1f);
                return objectHitted.collider;
            }
            return null;
        }

        protected virtual Vector3 GetTriggerPosition()
        {
            Vector3 finalPosition;
            finalPosition = transform.position + transform.forward * TriggerPositionOffset.z;
            finalPosition += transform.right * TriggerPositionOffset.x;
            finalPosition += transform.up * TriggerPositionOffset.y;
            return finalPosition;
        }

        protected virtual void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                Gizmos.color = GizmosColor;
                Gizmos.DrawRay(transform.position + Vector3.up * RayStartPointAltitude, transform.forward * 5);
            }
        }

    }
}