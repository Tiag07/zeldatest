﻿using Players;
using UnityEngine;

namespace CameraSystem
{
    public class FixedCamera : MonoBehaviour
    {
        private Camera myCamera;

        [SerializeField] private float transitionMoveSpeed = 20;
        [SerializeField] private float transitionRotateSpeed = 50;
        [SerializeField] private Transform waypointTarget;

        bool inTransition = false;

        private void Awake()
        {
            myCamera = GetComponent<Camera>();
        }

        private void OnEnable()
        {
            Player.PlayerAwakened += OnPlayerAwaken;
        }

        private void OnDisable()
        {
            Player.PlayerAwakened -= OnPlayerAwaken;
        }

        private void Start()
        {
            SetLookPoint(waypointTarget);
            myCamera.transform.position = waypointTarget.transform.position;
            myCamera.transform.eulerAngles = waypointTarget.transform.eulerAngles;
        }

        void OnPlayerAwaken(Player instantiatedPlayer)
        {
            instantiatedPlayer.CharMovement.PlayerCamera = myCamera;
        }

        public void SetLookPoint(Transform newTarget)
        {
            inTransition = true;
            waypointTarget = newTarget;
        }

        private void LateUpdate()
        {
            if (inTransition)
            {
                Time.timeScale = 0;
                Vector3 target = waypointTarget.position;
                myCamera.transform.position = Vector3.MoveTowards(myCamera.transform.position, target, transitionMoveSpeed * Time.unscaledDeltaTime);
                myCamera.transform.rotation = Quaternion.RotateTowards(myCamera.transform.rotation, waypointTarget.rotation, transitionRotateSpeed * Time.unscaledDeltaTime);

                if (myCamera.transform.position == target && myCamera.transform.rotation == waypointTarget.rotation)
                {
                    inTransition = false;
                    Time.timeScale = 1;
                }
            }

        }

    }
}