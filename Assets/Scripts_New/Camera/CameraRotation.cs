﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CameraSystem
{
    public class CameraRotation : MonoBehaviour
    {
        [SerializeField] private Vector3 value;
        public Vector3 Value { get => value; }

    }
}