﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Managers;
using Players;

namespace CameraSystem
{
    public class AdvancedDollyCamera : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera dollyCamera;
        private CinemachineTrackedDolly _trackedkDolly;
        private CinemachineHardLookAt _aimHardLookAt;

        private List<Collider> targets;

        [SerializeField] private Vector3 transitionDamping = new Vector3(5, 5, 5);
        [SerializeField] private float rotationSpeed = 20f;
        private Vector3 _originalTransitionDamping;
        private Vector3 _cameraRotation;

        [SerializeField] float transitionSpeed = 4;
        private bool _inDampingTransition = false;

        [SerializeField] private Transform lookAtTransform;
        private Transform _lookAtTarget = null;
        [SerializeField] private Transform followAtTransform;
        private Transform _followAtTarget = null;
        [SerializeField] private Vector3 followAtOffset = new Vector3();

        private enum LookAtPhaseEnum { InTransitionToLookAtCenterOfPlayers, LookingCenterOfPlayers, InTransitionToLookAtStaticDirection, LookingAtStaticDirection }
        private LookAtPhaseEnum lookAtPhase = LookAtPhaseEnum.InTransitionToLookAtCenterOfPlayers;

        private void Awake()
        {
            _trackedkDolly = dollyCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
            _aimHardLookAt = dollyCamera.GetCinemachineComponent<CinemachineHardLookAt>();
            _originalTransitionDamping = new Vector3(_trackedkDolly.m_XDamping, _trackedkDolly.m_YDamping, _trackedkDolly.m_ZDamping);
            dollyCamera.m_LookAt = lookAtTransform;
            dollyCamera.m_Follow = followAtTransform;
        }

        private void OnEnable()
        {
            GameManager.ListChanged += RefreshTargets;
        }

        private void OnDisable()
        {
            GameManager.ListChanged -= RefreshTargets;
        }

        private void LateUpdate()
        {
            RefreshLookAtTransform();
            RefreshFollowAtTransform();
            TrackTransition();
            LookAtTransition();
        }

        void LookAtTransition()
        {
            switch (lookAtPhase)
            {
                case LookAtPhaseEnum.InTransitionToLookAtCenterOfPlayers:
                    InTransitionToLookAtCenterOfPlayers();
                    break;

                case LookAtPhaseEnum.InTransitionToLookAtStaticDirection:
                    InTransitionToLookAtStaticDirection();
                    break;
            }

            void InTransitionToLookAtCenterOfPlayers()
            {
                var speed = transitionSpeed * rotationSpeed * Time.unscaledDeltaTime;
                var lookAtRotation = Quaternion.LookRotation(GetPlayersMidPoint() - dollyCamera.transform.position);
                dollyCamera.transform.rotation = Quaternion.RotateTowards(dollyCamera.transform.rotation, lookAtRotation, speed);

                if (dollyCamera.transform.rotation == lookAtRotation)
                {
                    lookAtPhase = LookAtPhaseEnum.LookingCenterOfPlayers;
                    _aimHardLookAt.enabled = true;

                }
            }

            void InTransitionToLookAtStaticDirection()
            {
                var speed = transitionSpeed * rotationSpeed * Time.unscaledDeltaTime;
                dollyCamera.transform.rotation = Quaternion.RotateTowards(dollyCamera.transform.rotation, Quaternion.Euler(_cameraRotation), speed);

                if (dollyCamera.transform.rotation == Quaternion.Euler(_cameraRotation))
                {
                    lookAtPhase = LookAtPhaseEnum.LookingAtStaticDirection;
                }
            }
        }

        void RefreshTargets(List<Player> playersInGame)
        {
            targets = new List<Collider>();
            foreach (var player in playersInGame)
            {
                targets.Add(player.CharMovement.CharController);
            }
        }

        public void SetCameraRotation(CameraRotation eulerAngles)
        {
            _aimHardLookAt.enabled = false;
            _cameraRotation = eulerAngles.Value;
            lookAtPhase = LookAtPhaseEnum.InTransitionToLookAtStaticDirection;
        }

        public void SetLookAtTarget(Transform target)
        {
            _lookAtTarget = target;
        }


        public void SetFollowAtTarget(Transform target)
        {
            _followAtTarget = target;
        }

        public void LookAtTheCenterOfPlayers()
        {
            _lookAtTarget = null;
            lookAtPhase = LookAtPhaseEnum.InTransitionToLookAtCenterOfPlayers;
        }

        public void FollowAtTheCenterOfPlayers()
        {
            _followAtTarget = null;
        }

        void RefreshLookAtTransform()
        {
            if (_lookAtTarget != null)
            {
                lookAtTransform.position = _lookAtTarget.position;
            }
            else
            {
                lookAtTransform.position = GetPlayersMidPoint();
            }
        }

        void RefreshFollowAtTransform()
        {
            if (_followAtTarget != null)
            {
                followAtTransform.position = _followAtTarget.position + followAtOffset;
            }
            else
            {
                followAtTransform.position = GetPlayersMidPoint() + followAtOffset;
            }
        }

        Vector3 GetPlayersMidPoint()
        {
            if (targets.Count == 0) return transform.position;
            var bounds = new Bounds(targets[0].bounds.center, Vector3.zero);
            foreach (var item in targets)
            {
                bounds.Encapsulate(item.bounds.center);
            }
            return bounds.center;
        }

        public void SetOffset_X(float xOffset) => _trackedkDolly.m_PathOffset.x = xOffset;
        public void SetOffset_Y(float yOffset) => _trackedkDolly.m_PathOffset.y = yOffset;
        public void SetOffset_Z(float zOffset) => _trackedkDolly.m_PathOffset.z = zOffset;
        public void ResetOffset() => _trackedkDolly.m_PathOffset = Vector3.zero;

        public void SetPath(CinemachineSmoothPath path)
        {
            if (_trackedkDolly.m_Path == path) return;
            _trackedkDolly.m_Path = path;

            _trackedkDolly.m_XDamping = transitionDamping.x;
            _trackedkDolly.m_YDamping = transitionDamping.y;
            _trackedkDolly.m_ZDamping = transitionDamping.z;

            _inDampingTransition = true;
        }

        void TrackTransition()
        {

            if (_inDampingTransition)
            {
                var speed = transitionSpeed * Time.unscaledDeltaTime;
                _trackedkDolly.m_XDamping = Mathf.MoveTowards(_trackedkDolly.m_XDamping, _originalTransitionDamping.x, speed);
                _trackedkDolly.m_YDamping = Mathf.MoveTowards(_trackedkDolly.m_YDamping, _originalTransitionDamping.y, speed);
                _trackedkDolly.m_ZDamping = Mathf.MoveTowards(_trackedkDolly.m_ZDamping, _originalTransitionDamping.z, speed);

                if (_trackedkDolly.m_XDamping == _originalTransitionDamping.x
                    && _trackedkDolly.m_YDamping == _originalTransitionDamping.y
                    && _trackedkDolly.m_ZDamping == _originalTransitionDamping.z)
                {
                    _inDampingTransition = false;
                }
            }
        }
    }
}