﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Players;

namespace CameraSystem
{
    [RequireComponent(typeof(Camera))]
    public class ThirdPersonCameraController : MonoBehaviour
    {
        [SerializeField] CinemachineFreeLook freeLookCam;
        [SerializeField] private float xRotateSpeed = 280;
        [SerializeField] private float yRotateSpeed = 2;

        public Player.PlayerNumberEnum PlayerForReferece;
        public Keymap CamKeymap;
        public Camera Cam { get; private set; }

        private Collider _targetCollider;
        private Transform _targetCenterPoint;

        public CinemachineFreeLook FreeLookCam { get => freeLookCam; }

        private void Awake()
        {
            Cam = GetComponent<Camera>();
        }

        public void SetPlayer(Player instantiatedPlayer)
        {
            if (instantiatedPlayer.PlayerNumber == PlayerForReferece)
            {
                instantiatedPlayer.CharMovement.PlayerCamera = Cam;

                _targetCollider = instantiatedPlayer.CharMovement.CharController;

                if (_targetCenterPoint == null)
                    _targetCenterPoint = new GameObject("Target_" + PlayerForReferece + "_CameraPoint").transform;

                FreeLookCam.Follow = _targetCenterPoint;
                FreeLookCam.LookAt = _targetCenterPoint;
            }
        }

        void Start()
        {
            FreeLookCam.m_XAxis.m_MaxSpeed = 0;
            FreeLookCam.m_YAxis.m_MaxSpeed = 0;
        }

        void LateUpdate()
        {
            RefreshTargetPoint();
            CameraRotation();
        }

        void RefreshTargetPoint()
        {
            _targetCenterPoint.position = _targetCollider.bounds.center;
        }

        void CameraRotation()
        {
            if (CamKeymap.CameraInputMode == Keymap.CameraInputModeEnum.Mouse)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    FreeLookCam.m_XAxis.m_MaxSpeed = xRotateSpeed;
                    FreeLookCam.m_YAxis.m_MaxSpeed = yRotateSpeed;
                }
                if (Input.GetMouseButtonUp(1))
                {
                    FreeLookCam.m_XAxis.m_MaxSpeed = 0;
                    FreeLookCam.m_YAxis.m_MaxSpeed = 0;
                }
            }

            if (CamKeymap.CameraInputMode == Keymap.CameraInputModeEnum.Buttons)
            {
                if (Input.GetKey(CamKeymap.KeyCameraUp))
                {
                    FreeLookCam.m_YAxis.Value += yRotateSpeed * 0.5f * Time.deltaTime;
                }
                if (Input.GetKey(CamKeymap.KeyCameraDown))
                {
                    FreeLookCam.m_YAxis.Value -= yRotateSpeed * 0.5f * Time.deltaTime;
                }
                if (Input.GetKey(CamKeymap.KeyCameraLeft))
                {
                    FreeLookCam.m_XAxis.Value -= xRotateSpeed * 0.5f * Time.deltaTime;
                }
                if (Input.GetKey(CamKeymap.KeyCameraRight))
                {
                    FreeLookCam.m_XAxis.Value += xRotateSpeed * 0.5f * Time.deltaTime;
                }

            }
        }
    }
}