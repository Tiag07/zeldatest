﻿using UnityEngine;
using System.Collections.Generic;
using Players;

namespace CameraSystem
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private ThirdPersonCameraController cameraPrefab;
        [SerializeField] private List<ThirdPersonCameraController> activeCameras;

        private void OnEnable()
        {
            Player.PlayerAwakened += OnPlayerAwaken;
        }

        private void OnDisable()
        {
            Player.PlayerAwakened -= OnPlayerAwaken;
        }

        void OnPlayerAwaken(Player instantiatedPlayer)
        {
            if (Camera.main != null) Camera.main.gameObject.SetActive(false);

            var newCamController = Instantiate(cameraPrefab);
            activeCameras.Add(newCamController);

            newCamController.CamKeymap = instantiatedPlayer.InputHandler.PlayerKeymap;
            newCamController.PlayerForReferece = instantiatedPlayer.PlayerNumber;

            RefreshCameraSettings(newCamController, newCamController.PlayerForReferece);
            newCamController.SetPlayer(instantiatedPlayer);
        }

        void RefreshCameraSettings(ThirdPersonCameraController camController, Player.PlayerNumberEnum playerNumber)
        {
            if (playerNumber == Player.PlayerNumberEnum.Player_1)
            {
                camController.Cam.gameObject.layer = TagsAndLayers.Layer_CameraP1;
                camController.FreeLookCam.gameObject.layer = TagsAndLayers.Layer_CameraP1;
                camController.Cam.cullingMask = LayerMaskUtility.RemoveLayerFromLayerMask(TagsAndLayers.Layer_CameraP2, camController.Cam.cullingMask);
            }
            if (playerNumber == Player.PlayerNumberEnum.Player_2)
            {
                camController.Cam.gameObject.layer = TagsAndLayers.Layer_CameraP2;
                camController.FreeLookCam.gameObject.layer = TagsAndLayers.Layer_CameraP2;
                camController.Cam.cullingMask = LayerMaskUtility.RemoveLayerFromLayerMask(TagsAndLayers.Layer_CameraP1, camController.Cam.cullingMask);
            }
            RefreshScreenSplit();
        }

        void RefreshScreenSplit()
        {
            if (activeCameras.Count == 1)
            {
                activeCameras[0].Cam.rect = new Rect(0, 0, 1, 1);
            }
            else if (activeCameras.Count == 2)
            {
                activeCameras[0].Cam.rect = new Rect(0, 0, 0.5f, 1);
                activeCameras[1].Cam.rect = new Rect(0.5f, 0, 0.5f, 1);
            }
        }

    }
}