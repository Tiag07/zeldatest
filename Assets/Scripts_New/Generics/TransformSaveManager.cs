﻿using SaveSystem;
using UnityEngine;

public class TransformSaveManager : MonoBehaviour
{
    [SerializeField] private TransformForSaving[] transformsForSaving = new TransformForSaving[0];
    private GameData loadedGameData;

    private void Start()
    {
        loadedGameData = SaveSystemJson.LoadGame();
        LoadObjects();
    }

    public void LoadObjects()
    {
        if (loadedGameData.transformDatas.Count == 0) return;

        var transformDatas = loadedGameData.transformDatas;
        foreach (var transfObj in transformsForSaving)
        {
            if (TransformExistsInData(transfObj.ObjectKey, out int existentObjectId))
            {
                var existentTransform = transformDatas[existentObjectId];
                transfObj.transform.position = existentTransform.TransPosition;
                transfObj.transform.eulerAngles = existentTransform.TransRotation;
            }
        }
    }

    public void SaveObjects()
    {
        var transformDatas = loadedGameData.transformDatas;
        foreach (var transfObj in transformsForSaving)
        {
            var newTransformData = new TransformSaveData();
            newTransformData.TransKey = transfObj.ObjectKey;
            newTransformData.TransPosition = transfObj.transform.position;
            newTransformData.TransRotation = transfObj.transform.eulerAngles;

            if (TransformExistsInData(transfObj.ObjectKey, out int existentObjectId))
            {
                transformDatas[existentObjectId] = newTransformData;
            }
            else
            {
                transformDatas.Add(newTransformData);
            }
        }
        SaveSystemJson.SaveGame(loadedGameData);
    }

    bool TransformExistsInData(string objectKey, out int existentDataId)
    {
        var transformDatas = loadedGameData.transformDatas;
        for (int i = 0; i < transformDatas.Count; i++)
        {
            if (transformDatas[i].TransKey == objectKey)
            {
                existentDataId = i;
                return true;
            }
        }
        existentDataId = 0;
        return false;
    }
}

[System.Serializable]
public class TransformSaveData
{
    public string TransKey;
    public Vector3 TransPosition;
    public Vector3 TransRotation;
}
