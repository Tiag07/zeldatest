﻿using UnityEngine;

public class SwitchGameObject : MonoBehaviour
{
    [SerializeField] GameObject obj;
    [SerializeField] bool disableOnStart = false;

    private void Start()
    {
        if(disableOnStart)
            obj.gameObject.SetActive(false);
        else
            obj.gameObject.SetActive(true);
    }
    public void Switch()
    {
        if (obj.gameObject.activeSelf)
            obj.gameObject.SetActive(false);
        else 
            obj.gameObject.SetActive(true);
    }
}
