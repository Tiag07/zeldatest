﻿using UnityEngine;

public class DisableRenderOnStart : MonoBehaviour
{
    private void Start()
    {
        GetComponent<MeshRenderer>().enabled = false;
    }
}
