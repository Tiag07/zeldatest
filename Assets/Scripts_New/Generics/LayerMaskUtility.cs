﻿using UnityEngine;

public class LayerMaskUtility
{
    public static bool Exists(int layerToCheck, LayerMask layerMask)
    {
        return layerMask == (layerMask | (1 << layerToCheck));
    }

    public static LayerMask RemoveLayerFromLayerMask(int layerToRemove, LayerMask currentLayerMask)
    {
        return  currentLayerMask & ~(1 << layerToRemove);
    }
}
