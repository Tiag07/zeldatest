﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Events;

public class ChangeScene : MonoBehaviour
{
    [SerializeField] GameScenes.Scenes sceneToLoad;
    [SerializeField] private float delayToLoadScene = 0;
    [SerializeField] private float fadeOutTime = 1f;
    private static bool LoadingInProgress = false;

    public static Action<float, UnityAction> FadeOutStarted;

    private void OnEnable()
    {
        LoadingInProgress = false;
    }

    public void LoadScene()
    {
        if (LoadingInProgress) return;

        Time.timeScale = 1;
        StartCoroutine(DelayCoroutine());

        IEnumerator DelayCoroutine()
        {
            LoadingInProgress = true;
            yield return new WaitForSeconds(delayToLoadScene);
            FadeOutStarted?.Invoke(fadeOutTime, null);
            yield return new WaitForSeconds(fadeOutTime);
            LoadNow();
        }
    }
    void LoadNow()
    {      
        SceneManager.LoadScene(GameScenes.GetSceneName(sceneToLoad));       
    }
}
