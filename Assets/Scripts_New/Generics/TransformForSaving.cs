﻿using UnityEngine;

public class TransformForSaving : MonoBehaviour
{
    [SerializeField] private string objectKey = "";
    public string ObjectKey { get => objectKey;}
}

