﻿using UnityEngine;

public class RotationLoop : MonoBehaviour
{
    [SerializeField] Vector3 rotationFactor;
    void Update()
    {
        transform.Rotate(rotationFactor * Time.deltaTime);
    }
}
