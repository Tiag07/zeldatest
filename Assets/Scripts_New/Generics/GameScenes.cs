﻿public class GameScenes
{
    public enum Scenes { MainMenu, PuzzleOfCrates, RobotStealth, VictoryScene }

    public static string GetSceneName(Scenes scene)
    {
        switch (scene)
        {
            case Scenes.MainMenu:
                return "Scene_0_MainMenu";

            case Scenes.PuzzleOfCrates:
                return "Scene_1_PuzzleOfCrates";

            case Scenes.RobotStealth:
                return "Scene_2_Stealth";

            case Scenes.VictoryScene:
                return "Scene_Final_VictoryScene";
        }

        return "Scene_0_MainMenu";
    }
}
