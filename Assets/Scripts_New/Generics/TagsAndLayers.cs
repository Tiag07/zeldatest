﻿using UnityEngine;


public class TagsAndLayers : MonoBehaviour
{
    public const int Layer_IgnoreRaycast = 2;
    public const int Layer_Character = 10;
    public const int Layer_CrouchArea = 12;
    public const int Layer_Collectable = 16;
    public const int Layer_GrabObjLimitWall = 13;
    public const int Layer_CameraP1 = 19;
    public const int Layer_CameraP2 = 20;

    public const string Tag_Player = "Player";

}
