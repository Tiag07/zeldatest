﻿using Character;
using UnityEditor;
using UnityEngine;

namespace CharacterAnimations
{
    [RequireComponent(typeof(Character_Movement))]
    [RequireComponent(typeof(Character_Graphics))]
    public class CharacterBasicAnimations : MonoBehaviour
    {
        protected Character_Graphics charGraphics;
        protected Character_Movement charMovement;
        protected Animator animator;
        protected void Awake()
        {
            RefreshEssentialComponents();
        }

        protected virtual void RefreshEssentialComponents()
        {
            charMovement = GetComponent<Character_Movement>();
            charGraphics = GetComponent<Character_Graphics>();
            charGraphics.CharacterAnimatorIntantiated += RefreshAnimatorComponents;
        }

        void RefreshAnimatorComponents(Animator anim)
        {
            animator = anim;
            AddAnimationEvents();
        }

        protected virtual void AddAnimationEvents()
        {
            if (animator == null) return;
            if (charMovement)
            {
                charMovement.Moving += Animate_Movement;
                charMovement.Grounded += Animate_Grounded;
            }
        }

        protected void Animate_Movement(Vector3 moveVelocity)
        {
            if (animator == null || Time.timeScale == 0) return;
            animator.SetFloat(AnimationHash.Movement, moveVelocity.magnitude);
        }

        protected void Animate_Grounded(bool isGrounded)
        {
            if (animator == null) return;
            animator.SetBool(AnimationHash.OnGround, isGrounded);
        }
    }
}