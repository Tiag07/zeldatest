﻿using Character;
using Enemies;
using UnityEngine;

namespace CharacterAnimations
{
    [RequireComponent(typeof(Character_Movement))]
    [RequireComponent(typeof(Guard))]
    public class GuardAnimations : CharacterBasicAnimations
    {
        private Guard _guard;

        protected override void RefreshEssentialComponents()
        {
            base.RefreshEssentialComponents();
            _guard = GetComponent<Guard>();
        }
        protected override void AddAnimationEvents()
        {
            base.AddAnimationEvents();
            _guard.TargetDetected += DetainTarget;
        }

        public void DetainTarget() => animator.SetBool(AnimationHash.Detain, true);
    }
}