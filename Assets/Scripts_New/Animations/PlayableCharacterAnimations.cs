﻿using CharacterMovementTypes;

namespace CharacterAnimations
{
    public class PlayableCharacterAnimations : CharacterBasicAnimations
    {
        protected override void AddAnimationEvents()
        {
            base.AddAnimationEvents();

            charMovement.MovementStateStarted += StartMovementTypeAnimation;
            charMovement.MovementStateFinished += FinishMovementTypeAnimation;
        }

        void StartMovementTypeAnimation(MovementType movementType)
        {
            if (animator == null) return;

            if (movementType is CrouchedMovement)
                animator.SetBool(AnimationHash.Crouched, true);

            else if (movementType is GrabMovement)
            {
                animator.SetBool(AnimationHash.Grabbing, true);
                GrabMovement grabMove = movementType as GrabMovement;

                grabMove.WaitingCommand += () => animator.SetFloat(AnimationHash.Movement, 0);
                grabMove.MovingObject += (yInput) => animator.SetFloat(AnimationHash.Movement, yInput);
                grabMove.Breathing += (breathing) => animator.SetBool(AnimationHash.GrabBreathing, breathing);
            }

        }

        void FinishMovementTypeAnimation(MovementType movementType)
        {
            if (animator == null) return;

            if (movementType is CrouchedMovement)
                animator.SetBool(AnimationHash.Crouched, false);

            else if (movementType is GrabMovement)
            {
                animator.SetBool(AnimationHash.Grabbing, false);
            }
        }
    }
}