﻿using UnityEngine;

class AnimationHash
{
    public static int Movement = Animator.StringToHash("Movement");
    public static int OnGround = Animator.StringToHash("Grounded");
    public static int Grabbing = Animator.StringToHash("Grabbing");
    public static int GrabBreathing = Animator.StringToHash("GrabBreathing");
    public static int Pushing = Animator.StringToHash("Pushing");
    public static int Pulling = Animator.StringToHash("Pulling");
    public static int Crouched = Animator.StringToHash("Crouched");

    //Guard
    public static int Detain = Animator.StringToHash("Detain");

}
