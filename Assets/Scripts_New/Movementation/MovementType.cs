﻿namespace CharacterMovementTypes
{
    public abstract class MovementType
    {
        public virtual bool AlternativeMovementation() => false;
        public virtual bool JumpEnabled() => true;
        public virtual float SpeedMultiplier() => 1f;

        public virtual bool CanExitFromThisState() => true;
        public virtual bool CanEnterInThisState() => true;

        public virtual void OnStateStarted() { }
        public virtual void OnStateUpdated() { }
        public virtual void OnStateFinished() { }
    }
}