﻿using Character;
using UnityEngine;

namespace CharacterMovementTypes
{
    public class StandingMovement : MovementType
    {
        private Character_Movement _charMovement;
        private Vector3 _charControllerOriginalCenter;
        private float _charControllerOriginalHeight;

        public StandingMovement(Character_Movement charMovement, Vector3 charControllerOriginalCenter, float charControllerOriginalHeight)
        {
            _charMovement = charMovement;
            _charControllerOriginalCenter = charControllerOriginalCenter;
            _charControllerOriginalHeight = charControllerOriginalHeight;
        }

        public override void OnStateStarted()
        {
            _charMovement.CharController.center = _charControllerOriginalCenter;
            _charMovement.CharController.height = _charControllerOriginalHeight;

            _charMovement.RefreshHeadColliderPosition();
            #region DefaultValues
            //_charMovement.CharController.slopeLimit = 45;
            //_charMovement.CharController.stepOffset = 0;
            //_charMovement.CharController.skinWidth = 0.02f;
            //_charMovement.CharController.minMoveDistance = 0.001f;
            //_charMovement.CharController.center = new Vector3(0, 0.95f, 0);
            //_charMovement.CharController.radius = 0.23f;
            //_charMovement.CharController.height = 1.9f;
            #endregion
        }
    }
}