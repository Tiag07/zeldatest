﻿namespace CharacterMovementTypes
{
    public class StaticMovement : MovementType
    {
        public override bool JumpEnabled()
        {
            return false;
        }
        public override float SpeedMultiplier()
        {
            return 0;
        }
    }
}