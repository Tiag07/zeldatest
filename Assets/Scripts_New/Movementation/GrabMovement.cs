﻿using Character;
using System;
using System.Collections;
using UnityEngine;

namespace CharacterMovementTypes
{
    public class GrabMovement : MovementType
    {
        public GrabMovement(Character_Movement charMovement, Interactor interactor, GrabbableObject grabbableObj)
        {
            _charMovement = charMovement;
            _grabbableObject = grabbableObj;
            _interactor = interactor;
        }

        private Character_Movement _charMovement;
        private Interactor _interactor;
        private GrabbableObject _grabbableObject;

        private Quaternion _grabStartRotation = Quaternion.identity;
        private Vector3 _grabStartPosition = Vector3.zero;
        private Vector3 _objectLastPosition = Vector3.zero;

        readonly float _pushDistance = 1f;
        readonly float _breathCooldownInSeconds = 1f;
        bool _isMoving = false;
        bool _isBreathing = false;

        public enum GrabState { WaitingCommand, MovingObject, MovingProcessInterrupted, Breathing }
        GrabState _grabState = GrabState.WaitingCommand;

        public Action WaitingCommand;
        public Action<float> MovingObject;
        public Action<bool> Breathing;

        public override bool AlternativeMovementation() => true;
        public override bool JumpEnabled() => false;
        public override float SpeedMultiplier() => 0.8f;

        public override bool CanEnterInThisState()
        {
            if (_charMovement.CurrentMoveType != _charMovement.StandingMovement) return false;
            if (_charMovement.IsGrounded == false) return false;
            return true;
        }

        public override void OnStateStarted()
        {
            _interactor.CanFindNewInteractables = false;
            _interactor.enabled = false;

            _charMovement.AutoMove_Interrupted += OnCharacterMoveInterrupted;
            _charMovement.AutoMove_OnDestination += OnCharacterPositioned;

            PrepareForGrabbing();
        }

        public override bool CanExitFromThisState()
        {
            if (_grabState == GrabState.WaitingCommand) return true;
            if (_grabState == GrabState.MovingProcessInterrupted) return true;
            return false;
        }

        public override void OnStateFinished()
        {
            _charMovement.MoveInputReceived -= MoveObject;
            _charMovement.AutoMove_Interrupted -= OnCharacterMoveInterrupted;
            _charMovement.AutoMove_OnDestination -= OnCharacterPositioned;

            _grabbableObject.IsBeingGrabbed = false;
            _interactor.CanFindNewInteractables = true;
            _interactor.enabled = true;
        }

        void PrepareForGrabbing()
        {
            RefreshGrabPositionAndRotationStartValues();
            _charMovement.MoveAutomatically(_grabStartPosition, CharacterIsNotGrounded, true);
        }

        void OnCharacterMoveInterrupted(Character_Movement charMovement)
        {
            InterruptProcess();
        }
        void OnCharacterPositioned(Character_Movement charMovement)
        {
            _charMovement.transform.rotation = _grabStartRotation;
            _charMovement.MoveInputReceived += MoveObject;

            _charMovement.InvokeMovementStateStarted();
            _grabbableObject.IsBeingGrabbed = true;
            _interactor.enabled = true;
        }

        void RefreshGrabPositionAndRotationStartValues()
        {
            Transform charTransform = _charMovement.transform;
            const float distanceFromGrabObject = -0.55f;

            if (charTransform.localPosition.x > _grabbableObject.transform.localPosition.x + _grabbableObject.Coll.bounds.extents.x)
            {
                _grabStartRotation = Quaternion.Euler(Vector3.up * 270);
                _grabStartPosition = new Vector3(_grabbableObject.Coll.bounds.max.x - distanceFromGrabObject, charTransform.position.y, _grabbableObject.transform.position.z);
            }
            else if (charTransform.localPosition.x < _grabbableObject.transform.localPosition.x - _grabbableObject.Coll.bounds.extents.x)
            {
                _grabStartRotation = Quaternion.Euler(Vector3.up * 90);
                _grabStartPosition = new Vector3(_grabbableObject.Coll.bounds.min.x + distanceFromGrabObject, charTransform.position.y, _grabbableObject.transform.position.z);
            }
            else if (charTransform.localPosition.z > _grabbableObject.transform.localPosition.z + _grabbableObject.Coll.bounds.extents.z)
            {
                _grabStartRotation = Quaternion.Euler(Vector3.up * 180);
                _grabStartPosition = new Vector3(_grabbableObject.transform.localPosition.x, charTransform.localPosition.y, _grabbableObject.Coll.bounds.max.z - distanceFromGrabObject);
            }
            else if (charTransform.localPosition.z < _grabbableObject.transform.localPosition.z - _grabbableObject.Coll.bounds.extents.z)
            {
                _grabStartRotation = Quaternion.Euler(Vector3.up * 0);
                _grabStartPosition = new Vector3(_grabbableObject.transform.localPosition.x, charTransform.localPosition.y, _grabbableObject.Coll.bounds.min.z + distanceFromGrabObject);
            }
        }

        private void MoveObject(float xAxis, float yAxis)
        {
            if (_grabState == GrabState.WaitingCommand)
            {
                if (ObjecIsFalling() || CharacterIsNotGrounded())
                {
                    InterruptProcess();
                }
                WaitingInputCommand();
            }
            else if (_grabState == GrabState.MovingObject && _isMoving == false)
            {
                _charMovement.StartCoroutine(MovingProcess(yAxis));
            }
            else if (_grabState == GrabState.Breathing && _isBreathing == false)
            {
                _charMovement.StartCoroutine(BreathCooldown());
            }

            void WaitingInputCommand()
            {
                WaitingCommand?.Invoke();

                if (yAxis != 0)
                    _grabState = GrabState.MovingObject;
            }

            IEnumerator MovingProcess(float yInput)
            {
                _isMoving = true;

                Vector3 destinationPoint = _grabbableObject.RBody.position + _charMovement.transform.forward * _pushDistance * yInput;

                Vector3 characterOffset = _charMovement.transform.position - _grabbableObject.transform.position;
                characterOffset.y = 0;

                _objectLastPosition = _grabbableObject.RBody.position;

                while (_grabbableObject.RBody.position != destinationPoint)
                {
                    if (ConditionsBeforeMovementAreCorrectly() == false)
                    {
                        InterruptProcess();
                        yield break;
                    }

                    TryToMoveObject();
                    AjustCharacterPosition();

                    if (ConditionsAfterMovementAreCorrectly() == false)
                    {
                        InterruptProcess();
                        yield break;
                    }

                    _objectLastPosition = _grabbableObject.RBody.position;
                    yield return new WaitForFixedUpdate();
                }

                _grabState = GrabState.Breathing;
                _isMoving = false;

                bool ConditionsBeforeMovementAreCorrectly()
                {
                    if (ObjecIsFalling()) return false;
                    if (CharacterIsNotGrounded()) return false;
                    if (yInput < 0 && ThereIsAnObjectBehindCharacter()) return false;
                    if (yInput > 0 && ThereIsOtherRigidbodyInFront()) return false;

                    return true;
                }

                void TryToMoveObject()
                {
                    _grabbableObject.RBody.MovePosition(Vector3.MoveTowards(_grabbableObject.RBody.position, destinationPoint, Time.deltaTime));
                }

                void AjustCharacterPosition()
                {
                    MovingObject?.Invoke(yInput);
                    Vector3 pos = _grabbableObject.transform.position + characterOffset;
                    pos.y = _charMovement.transform.position.y;
                    _charMovement.Teleport(pos);
                }

                bool ConditionsAfterMovementAreCorrectly()
                {
                    if (GrabObjectIsNotMoving()) return false;
                    if (yInput > 0 && CharacterCanNotPush()) return false;

                    return true;
                }
            }

            IEnumerator BreathCooldown()
            {
                _isBreathing = true;
                Breathing?.Invoke(true);

                yield return new WaitForSeconds(_breathCooldownInSeconds);
                _grabState = GrabState.WaitingCommand;

                _isBreathing = false;
                Breathing?.Invoke(false);
            }
        }

        bool GrabObjectIsNotMoving() => _objectLastPosition == _grabbableObject.RBody.position;

        bool CharacterCanNotPush()
        {
            var detectedObjects = _charMovement.GetObjectsInFrontOfFeets();
            foreach (var obj in detectedObjects)
            {
                if (obj.gameObject == _charMovement.gameObject || obj.gameObject == _grabbableObject.gameObject)
                    continue;
                return true;
            }
            return false;
        }


        bool CharacterIsNotGrounded() => !_charMovement.IsGrounded;
        bool ObjecIsFalling() => _grabbableObject.RBody.velocity.y < -1;
        bool ThereIsAnObjectBehindCharacter()
        {
            Vector3 checkSpherePosition = _charMovement.transform.position - _charMovement.transform.forward * 0.1f;
            checkSpherePosition.y += _charMovement.CharControllerOriginalCenter.y;
            float checkSphereRadius = _charMovement.CharController.radius + 0.1f;
            LayerMask layersCanInterruptProcess = -1;
            layersCanInterruptProcess = LayerMaskUtility.RemoveLayerFromLayerMask(TagsAndLayers.Layer_GrabObjLimitWall, layersCanInterruptProcess);

            Collider[] objectsDetected = Physics.OverlapSphere(checkSpherePosition, checkSphereRadius, layersCanInterruptProcess, QueryTriggerInteraction.Ignore);

            foreach (var obj in objectsDetected)
            {
                if (obj.gameObject != _charMovement.gameObject) return true;
            }
            return false;
        }
        bool ThereIsOtherRigidbodyInFront()
        {
            const float boxOffsetFactor = 0.1f;
            Vector3 boxPosition = _grabbableObject.transform.position + _charMovement.transform.forward * boxOffsetFactor;

            const float boxDecreaseFactor = 0.9f;
            Vector3 boxSize = _grabbableObject.Coll.bounds.size * boxDecreaseFactor * 0.5f;

            var objs = Physics.OverlapBox(boxPosition, boxSize, _grabbableObject.transform.rotation, -1, QueryTriggerInteraction.Ignore);

            foreach (var item in objs)
            {
                if (item.gameObject != _grabbableObject.gameObject) return true;
            }
            return false;
        }

        public void InterruptProcess()
        {
            _grabState = GrabState.MovingProcessInterrupted;
            _charMovement.DefaultMovementation(out bool successful);
        }
    }
}