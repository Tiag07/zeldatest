﻿using Character;
using UnityEngine;

namespace CharacterMovementTypes
{
    public class CrouchedMovement : MovementType
    {
        public CrouchedMovement(Character_Movement charMovement)
        {
            _charMovement = charMovement;
        }

        private Character_Movement _charMovement;

        public override bool CanEnterInThisState() => _charMovement.IsGrounded;

        public override void OnStateStarted()
        {
            const float sizeReductionFactor = 0.6f;
            _charMovement.CharController.center = _charMovement.CharControllerOriginalCenter * sizeReductionFactor;
            _charMovement.CharController.height = _charMovement.CharControllerOriginalHeight * sizeReductionFactor;

            _charMovement.RefreshHeadColliderPosition();
        }

        public override bool CanExitFromThisState()
        {
            if (IsThereSomethingAbovePlayer() || InCrouchArea())
                return false;
            else
                return true;

            bool IsThereSomethingAbovePlayer()
            {
                const float rayOffset = 0.2f;
                float rayDistance = _charMovement.CharController.height + rayOffset;

                if (Physics.Raycast(_charMovement.transform.position, Vector3.up, out RaycastHit hit, rayDistance, -1, QueryTriggerInteraction.Ignore))
                {
                    if (hit.transform.gameObject != _charMovement.transform.gameObject)
                        return true;
                }
                return false;
            }

            bool InCrouchArea()
            {
                LayerMask crouchArea = 1 << TagsAndLayers.Layer_CrouchArea;
                const float _crouchAreaCheckRadius = 0.1f;
                return Physics.CheckSphere(_charMovement.CharController.bounds.center, _crouchAreaCheckRadius, crouchArea, QueryTriggerInteraction.Collide);
            }

        }

        public override float SpeedMultiplier() => 0.4f;

        public override bool JumpEnabled() => false;
    }
}