﻿using Managers;
using Players;
using UnityEngine;
using UnityEngine.UI;

public class Menu_PlayerSelection : MonoBehaviour
{
    [SerializeField] PlayerManager playerManagerPrefab;
    [SerializeField] ChangeScene changeScene;

    [SerializeField] public enum GameMode { OnePlayer, TwoPlayers }
    [SerializeField] public GameMode gameMode;

    [Header("Characters")]
    [SerializeField] private Player[] prefabOfPlayers;
    private Player currentP1;
    private Player currentP2;
    [SerializeField] private int _currentId_P1 = 0;
    [SerializeField] private int _currentId_P2 = 1;


    [Header("UI")]
    [SerializeField] private GameObject panelPlayerTwo;
    [SerializeField] private Text txtCharNameP1;
    [SerializeField] private Text txtCharNameP2;

    private void Start()
    {
        currentP1 = prefabOfPlayers[0];
        currentP2 = prefabOfPlayers[1];
        RefreshCharacterP1();
        RefreshCharacterP2();
    }

    public void SetOnePlayerMode()
    {
        panelPlayerTwo.SetActive(false);
        gameMode = GameMode.OnePlayer;
    }

    public void SetTwoPlayersMode()
    {
        panelPlayerTwo.SetActive(true);
        gameMode = GameMode.TwoPlayers;
    }

    public void SetNextCharacterP1() 
    {
        _currentId_P1++;
        RefreshCharacterP1();
    }
    public void SetPreviousCharacterP1() 
    {
        _currentId_P1--;
        RefreshCharacterP1();
    }

    private void RefreshCharacterP1()
    {
        _currentId_P1 = Mathf.Clamp(_currentId_P1, 0, prefabOfPlayers.Length - 1);
        currentP1 = prefabOfPlayers[_currentId_P1];
        txtCharNameP1.text = currentP1.PlayerName;
    }

    public void SetNextCharacterP2()
    {
        _currentId_P2++;
        RefreshCharacterP2();
    }
    public void SetPreviousCharacterP2()
    {
        _currentId_P2--;       
        RefreshCharacterP2();
    }

    private void RefreshCharacterP2()
    {
        _currentId_P2 = Mathf.Clamp(_currentId_P2, 0, prefabOfPlayers.Length - 1);
        currentP2 = prefabOfPlayers[_currentId_P2];
        txtCharNameP2.text = currentP2.PlayerName;
    }

    public void StartGame()
    {
        if(PlayerManager.Instance == null)
        {
            Instantiate(playerManagerPrefab);
        }

        if(gameMode == GameMode.OnePlayer)
        {
            currentP2 = null;
        }

        PlayerManager.Instance.PlayerOnePrefab = currentP1;
        PlayerManager.Instance.PlayerTwoPrefab = currentP2;

        changeScene.LoadScene();
    }

    
}