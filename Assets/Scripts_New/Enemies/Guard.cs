﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.Events;
using Players;
using DetectionTriggers;

namespace Enemies
{
    public class Guard : MonoBehaviour
    {
        [SerializeField] private OverlapTrigger visionRayTrigger;
        [SerializeField] private UnityEvent onTargetDetained;

        private static bool GuardsEnabled = true;

        public Action TargetDetected;
        public static Action TargetIsBusted;

        private void OnEnable()
        {
            GuardsEnabled = true;
            StartCoroutine(UpdateAsync());
        }

        IEnumerator UpdateAsync()
        {
            while (GuardsEnabled)
            {
                var objDetected = visionRayTrigger.GetFirstObjectHitted();

                if (objDetected != null && objDetected.GetComponent<Player>() != null)
                {
                    DetainTheTarget(objDetected.gameObject);
                }

                yield return new WaitForSeconds(0.1f);
            }
        }

        public void DetainTheTarget(GameObject target)
        {
            StopAllGuards();
            TargetDetected?.Invoke();
            TargetIsBusted?.Invoke();

            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
            onTargetDetained.Invoke();
        }

        public static void StopAllGuards()
        {
            GuardsEnabled = false;
        }
    }
}