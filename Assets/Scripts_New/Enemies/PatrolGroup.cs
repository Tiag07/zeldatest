﻿using Character;
using System.Collections;
using UnityEngine;

namespace Enemies
{
    public class PatrolGroup : MonoBehaviour
    {
        public bool PatrolEnabled { get; set; } = true;

        [SerializeField] bool inverseRoute = false;
        [SerializeField] float beathTimeInSeconds = 2f;

        [SerializeField] Transform[] wayPoints;
        [SerializeField] Patroller[] patrollers;

        [System.Serializable]
        class Patroller
        {
            public Character_Movement CharMovement;
            public int StartWaypointId = 0;
            [HideInInspector] public int CurrentWayPoint = 0;
        }

        private void Start()
        {
            SetStartPositions();
        }

        void SetStartPositions()
        {
            foreach (var patroller in patrollers)
            {
                patroller.CharMovement.RefreshComponents();

                Vector3 startPosition = wayPoints[patroller.StartWaypointId].position;
                startPosition.y = patroller.CharMovement.transform.position.y;

                patroller.CharMovement.Teleport(startPosition);
                patroller.CharMovement.AutoMove_OnDestination += OnTheDestinedWaypoint;
                patroller.CurrentWayPoint = patroller.StartWaypointId;
                GoToNextWaypoint(patroller);
            }
        }

        void OnTheDestinedWaypoint(Character_Movement charMovement)
        {
            var patroller = CharacterMovementToPatroller(charMovement);
            StartCoroutine(BreathingCoroutine(patroller));
        }

        Patroller CharacterMovementToPatroller(Character_Movement charMovement)
        {
            foreach (var patroller in patrollers)
            {
                if (charMovement == patroller.CharMovement)
                    return patroller;
            }
            return null;
        }

        IEnumerator BreathingCoroutine(Patroller patroller)
        {
            yield return new WaitForSeconds(beathTimeInSeconds);

            if (PatrolEnabled)
            {
                GoToNextWaypoint(patroller);
            }
        }

        void GoToNextWaypoint(Patroller patrol)
        {
            int firstWayPoint = 0;
            int lastWayPoint = wayPoints.Length - 1;
            if (inverseRoute)
            {
                patrol.CurrentWayPoint--;
                if (patrol.CurrentWayPoint < firstWayPoint)
                    patrol.CurrentWayPoint = lastWayPoint;
            }
            else
            {
                patrol.CurrentWayPoint++;
                if (patrol.CurrentWayPoint > lastWayPoint)
                    patrol.CurrentWayPoint = firstWayPoint;
            }

            Vector3 destination = wayPoints[patrol.CurrentWayPoint].position;
            bool PatrolInterrupted() => !PatrolEnabled;

            patrol.CharMovement.MoveAutomatically(destination, PatrolInterrupted, true, 0.2f);
        }

    }
}