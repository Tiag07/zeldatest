﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterStats", menuName = "ScriptableObjects/Character Stats")]
public class CharacterStats : ScriptableObject
{
    [SerializeField] private float moveSpeed = 3.5f;
    [SerializeField] private float jumpForce = 2f;

    [SerializeField] private float gravity = -9.81f;
    [SerializeField] private float gravityForceWhenPlayerIsOnGround = -2f;

    public float MoveSpeed { get => moveSpeed;}
    public float JumpForce { get => jumpForce;}
    public float Gravity { get => gravity;}
    public float GravityForceWhenPlayerIsOnGround { get => gravityForceWhenPlayerIsOnGround;}
}
