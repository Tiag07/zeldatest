﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Input Profile", menuName = "ScriptableObjects/Keymap")]
public class Keymap : ScriptableObject
{
    [SerializeField] private KeyCode keyUp = KeyCode.W;
    [SerializeField] private KeyCode keyDown = KeyCode.S;
    [SerializeField] private KeyCode keyLeft = KeyCode.A;
    [SerializeField] private KeyCode keyRight = KeyCode.D;
    [SerializeField] private KeyCode keyJump = KeyCode.Space;
    [SerializeField] private KeyCode keyInteract = KeyCode.E;
    [SerializeField] private KeyCode keyStart = KeyCode.Escape;

    public enum CameraInputModeEnum { Mouse, Buttons }
    [SerializeField] private CameraInputModeEnum cameraInputMode = CameraInputModeEnum.Buttons;

    [SerializeField] private KeyCode keyCameraUp = KeyCode.None;
    [SerializeField] private KeyCode keyCameraDown = KeyCode.None;
    [SerializeField] private KeyCode keyCameraLeft = KeyCode.None;
    [SerializeField] private KeyCode keyCameraRight = KeyCode.None;

    public KeyCode KeyUp { get => keyUp; }
    public KeyCode KeyDown { get => keyDown; }
    public KeyCode KeyLeft { get => keyLeft; }
    public KeyCode KeyRight { get => keyRight; }
    public KeyCode KeyJump { get => keyJump; }
    public KeyCode KeyInteract { get => keyInteract; }
    public KeyCode KeyStart { get => keyStart; }

    public CameraInputModeEnum CameraInputMode { get => cameraInputMode; }

    public KeyCode KeyCameraUp { get => keyCameraUp; }
    public KeyCode KeyCameraDown { get => keyCameraDown; }
    public KeyCode KeyCameraLeft { get => keyCameraLeft; }
    public KeyCode KeyCameraRight { get => keyCameraRight; }
}
