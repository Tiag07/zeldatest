﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shared Layer Mask", menuName = "ScriptableObjects/Shared Layer Mask")]

public class SharedLayerMask : ScriptableObject
{
    [SerializeField] private LayerMask layers;

    public LayerMask Layers { get => layers; }
}
