﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TransformSaveManager))]
public class TransformSaveManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (Application.isPlaying)
        {
            TransformSaveManager transformSave = (TransformSaveManager)target;
            if (GUILayout.Button("Save Transforms"))
            {
                transformSave.SaveObjects();
            }
            if (GUILayout.Button("Load Transforms"))
            {
                transformSave.SaveObjects();
            }
        }

    }
}