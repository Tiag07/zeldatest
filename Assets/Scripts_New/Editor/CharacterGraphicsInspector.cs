﻿using UnityEngine;
using UnityEditor;
using Character;

[CustomEditor(typeof(Character_Graphics))]
public sealed class CharacterGraphicsInspector : Editor
{
    SerializedProperty modelPrefab;
    SerializedProperty animController;

    private void OnEnable()
    {
        modelPrefab = serializedObject.FindProperty("modelPrefab");
        animController = serializedObject.FindProperty("animController");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        Character_Graphics charGraphics = (Character_Graphics)target;
        bool isPrefabInstance = PrefabUtility.IsPartOfAnyPrefab(charGraphics);

        if (Application.isPlaying)
        {
            EditorGUILayout.LabelField("You can not modify in Play Mode.");
        }
        else if (isPrefabInstance && ModelInPrefab() != null)
        {
            EditorGUILayout.LabelField("You can not modify a prefab instance.");
            EditorGUILayout.LabelField("Open prefab to edit it.");
        }
        else
        {
            ShowOptions(charGraphics);
        }

        Character_ModelInstance ModelInPrefab()
        {
            var prefab = PrefabUtility.GetCorrespondingObjectFromSource(charGraphics);
            if(prefab != null)
            {
                return prefab.GetComponentInChildren<Character_ModelInstance>();
            }
            return null;
        }
    }

    private void ShowOptions(Character_Graphics charGraphics)
    {
        EditorGUILayout.PropertyField(modelPrefab);
        EditorGUILayout.PropertyField(animController);
        if (GUILayout.Button("Apply"))
        {
            charGraphics.RefreshModelAndAnimator();
        }
        serializedObject.ApplyModifiedProperties();
    }
}

public class RefreshAll : EditorWindow
{
    [MenuItem("CharacterGraphics/Refresh all non-prefabs")]
    public static void RefreshCharacters()
    {
        var characters = FindObjectsOfType<Character_Graphics>();

        foreach (var character in characters)
        {
            bool isPrefabInstance = PrefabUtility.IsPartOfAnyPrefab(character);
            if (isPrefabInstance == false)
                character.RefreshModelAndAnimator();
        }
    }
}
