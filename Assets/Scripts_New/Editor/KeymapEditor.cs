﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Keymap))]

public class KeymapEditor : Editor
{
    SerializedProperty keyUp;
    SerializedProperty keyDown;
    SerializedProperty keyLeft;
    SerializedProperty keyRight;
    SerializedProperty keyJump;
    SerializedProperty keyInteract;
    SerializedProperty keyStart;

    SerializedProperty cameraInputMode;
    SerializedProperty keyCameraUp;
    SerializedProperty keyCameraDown;
    SerializedProperty keyCameraLeft;
    SerializedProperty keyCameraRight;

    private void OnEnable()
    {
        keyUp = serializedObject.FindProperty("keyUp");
        keyDown = serializedObject.FindProperty("keyDown");
        keyLeft = serializedObject.FindProperty("keyLeft");
        keyRight = serializedObject.FindProperty("keyRight");
        keyJump = serializedObject.FindProperty("keyJump");
        keyInteract = serializedObject.FindProperty("keyInteract");
        keyStart = serializedObject.FindProperty("keyStart");

        cameraInputMode = serializedObject.FindProperty("cameraInputMode");

        keyCameraUp = serializedObject.FindProperty("keyCameraUp");
        keyCameraDown = serializedObject.FindProperty("keyCameraDown");
        keyCameraLeft = serializedObject.FindProperty("keyCameraLeft");
        keyCameraRight = serializedObject.FindProperty("keyCameraRight");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        Keymap keymap = (Keymap)target;

        EditorGUILayout.PropertyField(keyUp);
        EditorGUILayout.PropertyField(keyDown);
        EditorGUILayout.PropertyField(keyLeft);
        EditorGUILayout.PropertyField(keyRight);
        EditorGUILayout.PropertyField(keyJump);
        EditorGUILayout.PropertyField(keyInteract);
        EditorGUILayout.PropertyField(keyStart);

        EditorGUILayout.LabelField("Camera Settings");
        EditorGUILayout.PropertyField(cameraInputMode);

        if (keymap.CameraInputMode == Keymap.CameraInputModeEnum.Buttons)
        {
            EditorGUILayout.PropertyField(keyCameraUp);
            EditorGUILayout.PropertyField(keyCameraDown);
            EditorGUILayout.PropertyField(keyCameraLeft);
            EditorGUILayout.PropertyField(keyCameraRight);
        }
        serializedObject.ApplyModifiedProperties();
    }
}
