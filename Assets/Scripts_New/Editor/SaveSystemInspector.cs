﻿using SaveSystem;
using UnityEditor;

public class SaveSystemInspector : EditorWindow
{
    [MenuItem("SaveSystem/Clear Data")]
    public static void ClearData()
    {
        SaveSystemJson.ClearData();
    }
}
