﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Players;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        public List<Player> PlayersInGame { get; private set; } = new List<Player>();
        public static GameManager Instance;
        public bool GameIsPaused { get; private set; } = false;

        public static Action<List<Player>> ListChanged;

        [SerializeField] private Transform spawnPoint_P1;
        [SerializeField] private Transform spawnPoint_P2;

        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            Player.PlayerAwakened += OnPlayerAwakened;
        }

        private void OnDisable()
        {
            Player.PlayerAwakened -= OnPlayerAwakened;
        }

        public void OnPlayerAwakened(Player newPlayer)
        {
            if (PlayersInGame.Contains(newPlayer) == false)
            {
                PlayersInGame.Add(newPlayer);
                ListChanged?.Invoke(PlayersInGame);
            }

            if (newPlayer.PlayerNumber == Player.PlayerNumberEnum.Player_1)
                newPlayer.CharMovement.Teleport(spawnPoint_P1.position);

            else if (newPlayer.PlayerNumber == Player.PlayerNumberEnum.Player_2)
                newPlayer.CharMovement.Teleport(spawnPoint_P2.position);
        }

        public void PauseGame()
        {
            GameIsPaused = true;
            StopAllPlayers();
            Time.timeScale = 0;
        }

        public void ResumeGame()
        {
            GameIsPaused = false;
            ResumeAllPlayers();
            Time.timeScale = 1;
        }

        public void StopAllPlayers()
        {
            foreach (var player in PlayersInGame)
            {
                player.StopPayer();
            }
        }

        public void ResumeAllPlayers()
        {
            foreach (var player in PlayersInGame)
            {
                player.ResumePayer();
            }
        }
    }
}