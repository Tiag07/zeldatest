﻿using Players;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class PlayerManager : MonoBehaviour
    {
        public static PlayerManager Instance { get; private set; }
        public Player PlayerOnePrefab;
        public Player PlayerTwoPrefab;

        [SerializeField] private Keymap KeymapP1;
        [SerializeField] private Keymap KeymapP2;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                gameObject.SetActive(false);
            }

            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (FindObjectOfType<GameManager>() != null)
            {
                InstantiatePlayers();
            }
        }

        void InstantiatePlayers()
        {
            if (PlayerOnePrefab != null)
            {
                InstantiatePlayer(PlayerOnePrefab, Player.PlayerNumberEnum.Player_1, KeymapP1);
            }

            if (PlayerTwoPrefab != null)
            {
                InstantiatePlayer(PlayerTwoPrefab, Player.PlayerNumberEnum.Player_2, KeymapP2);
            }

            void InstantiatePlayer(Player player, Player.PlayerNumberEnum playerNumber, Keymap playerKeymap)
            {
                var newPlayer = Instantiate(player);
                newPlayer.PlayerNumber = playerNumber;
                newPlayer.InputHandler.PlayerKeymap = playerKeymap;
            }
        }

    }
}