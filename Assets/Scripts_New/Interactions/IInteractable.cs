﻿using UnityEngine;
public interface IInteractable
{
    string ActionName { get; }
    void Interact(Interactor interactor);   
}
