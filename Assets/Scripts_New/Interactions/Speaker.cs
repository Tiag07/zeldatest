﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Speaker : MonoBehaviour, IInteractable
{
    public string ActionName => "Talk";

    [SerializeField] private Speech[] speeches = new Speech[1];
    
    public static Action<Speech[]> Speaking;
    public static Action SpeechFinished;
    
    public void Interact(Interactor interactor)
    {
        if (speeches.Length == 0) return;
        Speaking.Invoke(speeches);
    }
}

[Serializable]
public class Speech
{
    [TextArea(1, 2)] [SerializeField] private string messageText = "Write a message...";
    [SerializeField] private float durationInSeconds = 2.5f;

    public string MessageText { get => messageText; }
    public float DurationInSeconds { get => durationInSeconds; }
}
