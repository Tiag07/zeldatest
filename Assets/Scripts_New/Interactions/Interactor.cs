﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Interactor : MonoBehaviour
{
    public Action<string, Collider> InteractableDetected;
    public Action Interacted;
    [HideInInspector] public bool InteractionsEnabled = true;
    [HideInInspector] public bool CanFindNewInteractables = true;
    protected IInteractable CurrentInteractable;
}
