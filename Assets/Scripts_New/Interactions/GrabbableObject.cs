﻿using UnityEngine;
using Character;

namespace CharacterMovementTypes
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class GrabbableObject : MonoBehaviour, IInteractable
    {
        public string ActionName => "Grab";
        public bool InteractionEnabled { get => !IsBeingGrabbed; }

        public Collider Coll { get; private set; }
        public Rigidbody RBody { get; private set; }
        public bool IsBeingGrabbed = false;

        private void Start()
        {
            Coll = GetComponent<Collider>();
            RBody = GetComponent<Rigidbody>();
        }

        public void Interact(Interactor interactor)
        {
            var charMovement = interactor.GetComponent<Character_Movement>();
            var currentGrabMovement = new GrabMovement(charMovement, interactor, this);

            if (IsBeingGrabbed == false)
                charMovement.SwitchMovementState(currentGrabMovement, out bool successful, true, false);
            else
                charMovement.DefaultMovementation(out bool successful);
        }

    }
}