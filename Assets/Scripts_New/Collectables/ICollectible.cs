﻿using UnityEngine;

public interface ICollectible
{
    void Collected(ICollector collector);
}

