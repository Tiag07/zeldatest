﻿using UnityEngine;

public class Crystal : MonoBehaviour, ICollectible
{
    public int CrystalValue { get => crystalValue; private set { } }
    [SerializeField] private int crystalValue = 1;

    [SerializeField] private Color crystalColor = Color.yellow;
    const string _matColorPropertyName = "_Color";

    private void Start()
    {
        RefreshColor();
    }

    void RefreshColor()
    {
        Renderer myRenderer = GetComponent<Renderer>();
        MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();

        propertyBlock.SetColor(_matColorPropertyName, crystalColor);
        myRenderer.SetPropertyBlock(propertyBlock);
    }

    public void Collected(ICollector collector)
    {
        collector.Inventory().AddCrystals(crystalValue);
        gameObject.SetActive(false);
    }
}
