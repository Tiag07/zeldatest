﻿using System.Collections.Generic;

namespace SaveSystem
{
    [System.Serializable]
    public class GameData
    {
        public List<TransformSaveData> transformDatas = new List<TransformSaveData>();
    }
}