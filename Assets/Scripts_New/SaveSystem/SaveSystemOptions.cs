﻿using UnityEngine;

namespace SaveSystem
{
    public class SaveSystemOptions : MonoBehaviour
    {
        public void ClearData()
        {
            SaveSystemJson.ClearData();
        }
    }
}