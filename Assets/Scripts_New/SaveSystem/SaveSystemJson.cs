﻿using System.IO;
using UnityEngine;

namespace SaveSystem
{
    public class SaveSystemJson : MonoBehaviour
    {
        public static string directory = "/SaveData/";
        public static string fileName = "zeldaTestProgress.data";

        public static void SaveGame(GameData saveData)
        {
            string dir = Application.persistentDataPath + directory;

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            string json = JsonUtility.ToJson(saveData);
            File.WriteAllText(dir + fileName, json);
            Debug.Log(string.Concat("Data saved in: ", dir));
        }
        public static GameData LoadGame()
        {
            string fullPath = Application.persistentDataPath + directory + fileName;

            if (File.Exists(fullPath))
            {
                GameData saveData = new GameData();
                string json = File.ReadAllText(fullPath);
                saveData = JsonUtility.FromJson<GameData>(json);
                Debug.Log(string.Concat("Data loaded from: ", fullPath));
                return saveData;
            }
            else return new GameData();
        }

        public static void ClearData()
        {
            string fullPath = Application.persistentDataPath + directory + fileName;
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }

    }
}