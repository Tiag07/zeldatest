﻿using Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerUI
{
    public class UI_PanelBusted : MonoBehaviour
    {
        [SerializeField] private GameObject panelBusted;
        private void OnEnable()
        {
            Guard.TargetIsBusted += ShowPanel;
        }

        private void OnDisable()
        {
            Guard.TargetIsBusted -= ShowPanel;
        }

        private void Start()
        {
            panelBusted.SetActive(false);
        }

        void ShowPanel()
        {
            panelBusted.SetActive(true);
        }
    }
}