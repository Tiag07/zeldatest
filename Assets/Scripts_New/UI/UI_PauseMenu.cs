﻿using Managers;
using Players;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerUI
{
    public class UI_PauseMenu : MonoBehaviour
    {
        [SerializeField] GameObject pausePanel;

        private void OnEnable()
        {
            PlayerInputHandler.StartPressed += PauseAndResumeGame;
        }

        private void OnDisable()
        {
            PlayerInputHandler.StartPressed -= PauseAndResumeGame;
        }

        public void PauseAndResumeGame()
        {
            if (GameManager.Instance.GameIsPaused == false)
            {
                GameManager.Instance.PauseGame();
                pausePanel.SetActive(true);
            }
            else
            {
                GameManager.Instance.ResumeGame();
                pausePanel.SetActive(false);
            }
        }


    }
}