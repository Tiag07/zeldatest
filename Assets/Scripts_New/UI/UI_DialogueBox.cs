﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace PlayerUI
{
    public class UI_DialogueBox : MonoBehaviour
    {
        [SerializeField] GameObject panelDialogue;
        [SerializeField] Text txtMessage;
        bool _messageInProgress = false;

        private void OnEnable()
        {
            Speaker.Speaking += ShowDialogue;
            Speaker.SpeechFinished += CloseDialogue;
        }
        private void OnDisable()
        {
            Speaker.Speaking -= ShowDialogue;
            Speaker.SpeechFinished -= CloseDialogue;
        }

        private void Start()
        {
            panelDialogue.SetActive(false);
        }

        public void ShowDialogue(Speech[] speechs)
        {
            if (_messageInProgress) return;
            panelDialogue.SetActive(true);
            StartCoroutine(DialogueCoroutine(speechs));
        }

        IEnumerator DialogueCoroutine(Speech[] speechs)
        {
            _messageInProgress = true;
            foreach (var speech in speechs)
            {
                txtMessage.text = speech.MessageText;
                yield return new WaitForSeconds(speech.DurationInSeconds);
            }
            CloseDialogue();
        }

        public void CloseDialogue()
        {
            txtMessage.text = "";
            panelDialogue.SetActive(false);
            _messageInProgress = false;
        }
    }
}