﻿using Players;
using UnityEngine;
using UnityEngine.UI;

namespace PlayerUI
{
    public class UI_PlayerInfo : MonoBehaviour
    {
        [SerializeField] private Player.PlayerNumberEnum playerForReferece;
        private Player _player;
        [SerializeField] private GameObject panel;
        [SerializeField] private Text txtPlayerName;
        [SerializeField] private Text txtCrystalCount;
        [SerializeField] private Text txtInteractorActionName;
        [SerializeField] private ParticleSystem ptcInteractableArrow;

        private void Awake()
        {
            panel.gameObject.SetActive(false);
            ptcInteractableArrow.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            Player.PlayerAwakened += OnPlayerAwakened;
        }

        private void OnDisable()
        {
            Player.PlayerAwakened -= OnPlayerAwakened;
            RemovePlayerEvents();
        }

        void OnPlayerAwakened(Player instantiatedPlayer)
        {
            if (instantiatedPlayer.PlayerNumber == playerForReferece)
            {
                _player = instantiatedPlayer;
                txtPlayerName.text = _player.PlayerName;
                panel.gameObject.SetActive(true);
                AddPlayerEvents();
            }
        }

        void AddPlayerEvents()
        {
            _player.CharInteractions.InteractableDetected += OnInteractableDetected;
            _player.CharInteractions.Interacted += OnInteracted;
            _player.CharInventory.CrystalValueChanged += RefreshCrystalCount;
        }

        void RemovePlayerEvents()
        {
            if (_player == null) return;
            _player.CharInteractions.InteractableDetected -= OnInteractableDetected;
            _player.CharInteractions.Interacted -= OnInteracted;
            _player.CharInventory.CrystalValueChanged -= RefreshCrystalCount;
        }

        public void RefreshCrystalCount(int totalCrystals)
        {
            txtCrystalCount.text = totalCrystals.ToString("000");
        }

        void OnInteractableDetected(string actionName, Collider interactableCollider)
        {
            txtInteractorActionName.text = actionName;

            if (interactableCollider == null)
            {
                ptcInteractableArrow.gameObject.SetActive(false);
            }
            else
            {
                Vector3 colliderTopPosition = interactableCollider.bounds.center + (Vector3.up * interactableCollider.bounds.size.y / 2f);
                float distanceFromTop = 0.6f;

                ptcInteractableArrow.transform.position = colliderTopPosition + (Vector3.up * distanceFromTop);

                ptcInteractableArrow.gameObject.SetActive(true);
                ptcInteractableArrow.Play();
            }
        }

        void OnInteracted()
        {
            txtInteractorActionName.text = "";
            ptcInteractableArrow.gameObject.SetActive(false);
        }
    }
}