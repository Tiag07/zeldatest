﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PlayerUI
{
    public class UI_Manager : MonoBehaviour
    {

        [SerializeField] private Image imgFade;

        private void OnEnable()
        {
            ChangeScene.FadeOutStarted += FadeOut;
        }

        private void OnDisable()
        {
            ChangeScene.FadeOutStarted -= FadeOut;
        }

        private void Start()
        {
            FadeIn();
        }

        public void FadeIn(float fadeTime = 1f, UnityAction onComplete = null)
        {
            StartCoroutine(FadeCoroutine(1, 0, fadeTime, onComplete));
        }

        public void FadeOut(float fadeTime = 1f, UnityAction onComplete = null)
        {
            StartCoroutine(FadeCoroutine(0, 1, fadeTime, onComplete));
        }

        IEnumerator FadeCoroutine(float startAlpha, float finalAlpha, float fadeTime = 1f, UnityAction onComplete = null)
        {
            imgFade.CrossFadeAlpha(startAlpha, 0, false);
            imgFade.gameObject.SetActive(true);
            imgFade.CrossFadeAlpha(finalAlpha, fadeTime, false);
            yield return new WaitForSeconds(fadeTime);
            onComplete?.Invoke();
        }

    }
}